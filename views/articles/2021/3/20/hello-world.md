# Hello World!

Hi, there! I'm just a hello world article.

![Hello](assets/hello.webp)

Let's check out if code syntax highlighting works too:

```scala
// scala 2
object Main extends App {
  println("Hello, world!")
}
```

```scala
// scala 3
@main def hello = println("Hello, world!")
```

```haskell
-- haskell
main :: IO ()
main = putStrLn "Hello, world!"
```

```rust
// rust
fn main() {
  println!("Hello, world!");
}
```

```cpp
// c++
#include <iostream>

int main() {
  std::cout << "Hello, world!" << std::endl;
  return 0;
}
```

```bash
# bash
echo "Hello, world!"
```

![Awesome](assets/awesome.webp)