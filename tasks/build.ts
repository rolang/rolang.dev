import {init} from './init'
import fs from 'fs-extra'
import favicon from '../config/favicons'
import {renderAll as renderDocs} from './docs'

init()
  .then(() => renderDocs())
  .then(() => fs.remove(favicon.output.html))
  .then(() => console.info('Build successful'))