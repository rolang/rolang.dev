import {buildDir, srcDir, isProduction, articles} from '../config'
import fs from 'fs-extra'
import iconsConfig from '../config/favicons'
import createIcons from 'app-ico'
import path from "path";
import {optimize} from 'svgo'
import {renderAll as renderScss} from "./css";

const readdirSync = (dir: string): fs.Dirent[] => fs.readdirSync(dir, {withFileTypes: true})

type Asset = {
  srcPath: string
  distPath: string
  content: () => Buffer
}

function toAsset(srcDir: string, distDir: string, dirent: fs.Dirent): Asset {
  const srcPath = path.join(srcDir, dirent.name)

  return {
    srcPath,
    distPath: path.join(distDir, dirent.name),
    content: () => fs.readFileSync(srcPath)
  }
}

function commonAssets(): Asset[] {
  return readAssets(srcDir('assets'), buildDir('assets'))
}

function articleAssets(): Asset[] {
  return articles().flatMap(article => {
    const srcDir = path.join(article.srcDir, 'assets')
    const distDir = buildDir(path.join(path.dirname(article.distFilePath), 'assets'))

    if (fs.existsSync(srcDir)) {
      return readAssets(srcDir, distDir)
    } else {
      return []
    }
  })
}

function readAssets(srcDir: string, distDir: string): Asset[] {
  const recursiveRead = (from: fs.Dirent[], result: Asset[]): Asset[] => {
    if (from.length > 0) {
      const [current, rest] = [from[0], from.splice(1)]

      if (current.isDirectory()) {
        const dirFiles = readdirSync(current.name)
        return recursiveRead(dirFiles.concat(rest), result)
      } else {
        result.push(toAsset(srcDir, distDir, current))
        return recursiveRead(rest, result);
      }
    } else {
      return result
    }
  }

  return recursiveRead(readdirSync(srcDir), [])
}

function getAllAssets(): Asset[] {
  const allAssets = commonAssets().concat(articleAssets())

  if (isProduction) {
    return allAssets.map(a => {
      if (a.srcPath.endsWith('.svg')) {
        const optimized = optimize(a.content()).data
        a.content = () => optimized
      }
      return a
    })
  } else {
    return allAssets
  }
}

function copyAssets() {
  getAllAssets().forEach(a => {
    fs.ensureDirSync(path.dirname(a.distPath))
    fs.writeFileSync(a.distPath, a.content())
  })
}

export const init = (): Promise<unknown> =>
  fs.remove(buildDir())
    .then(() => copyAssets())
    .then(() => createIcons(iconsConfig))
    .then(() => renderScss())