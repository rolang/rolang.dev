import fs from 'fs-extra'

import {cssDir} from '../config'
import {render} from 'sass'
import {sassConf, SassFileConf} from "../config"
import postcss, {Result} from "postcss"
import autoprefixer from "autoprefixer"
import cssnano from "cssnano"

const plugins = [autoprefixer, cssnano]

export const renderFile = (sassFileConf: SassFileConf): Promise<string> =>
  fs.ensureDir(cssDir()).then(() => {
    return new Promise((resolve, reject) => {
      render(sassFileConf, (err, res) => {
        if (err) {
          reject(err)
          return
        }

        postcss(plugins).process(res.css, {from: sassFileConf.outFile}).async()
          .then((r: Result) => fs.writeFile(sassFileConf.outFile, r.css))
          .then(() => resolve(res.css.toString()), reject)
      })
    })
  })

export const renderAll = () => Promise.all(sassConf.files.map(renderFile))
