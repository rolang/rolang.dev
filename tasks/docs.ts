import fs from 'fs-extra'
import pug from 'pug'
import {documentData} from '../config'
import {buildDir} from "../config";
import {allDocuments, Document} from "../config/pug";
import path from 'path'

const renderPage = (doc: Document): Promise<string> =>
  new Promise((resolve, reject) =>
    pug.renderFile(doc.srcFilePath, documentData(doc), (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    }))

export const renderAll = (): Promise<void[]> => {
  const renderPages =
    allDocuments().map(p => {
      const distFilePath = buildDir(p.distFilePath)
      fs.ensureDirSync(path.dirname(distFilePath))


      renderPage(p).then(html => fs.writeFile(distFilePath, html))
    })

  return Promise.all(renderPages)
}
