import path from 'path'
import express from 'express'

import {documentData} from "./config";
import {AddressInfo} from "net";
import {buildDir} from "./config";
import {allDocuments} from './config/pug'
import {init} from './tasks/init'

const port = process.env.PORT || 3000
const app = express()

app.set('view engine', 'pug')

allDocuments().forEach(doc => {
  app.get(doc.uri, (req, res) =>
    init().then(_ => res.render(path.relative('views', doc.srcFilePath), documentData(doc)))
  )
})

app.use(express.static(buildDir()))

init().then(() => {
  const server = app.listen(port)
  server.on("listening", () => {
      const addr: AddressInfo = server.address() as AddressInfo
      console.log(`Listening on port ${addr.port}!`)
    }
  )
})