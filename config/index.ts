import {Options} from "sass";
import {cssDir, sassDir} from "./paths";
import {Article, data, isArticle, Page} from './pug'

export const isProduction = process.env.NODE_ENV === 'production'

export interface SassConf {
  files: SassFileConf[]
}

export interface SassFileConf extends Options {
  file: string
  outFile: string
}

export const sassConf: SassConf = {
  files: [
    {
      file: sassDir('style.scss'),
      outFile: cssDir('style.css')
    },
    {
      file: sassDir('cv.scss'),
      outFile: cssDir('cv.css')
    }
  ]
}

export * from './paths'
export function documentData(document: Page | Article) {
  const baseData = data(isProduction)
  return {...baseData, ...(isArticle(document) ? baseData.articleData(document) : {})}
}
export {pages, articles} from './pug'