module.exports = ctx => ({
    replace: true,
    plugins: {
        'autoprefixer': {},
        'cssnano': ctx.env === 'prd' ? {} : false
    }
});
