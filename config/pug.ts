import {buildDir, srcDir} from "./paths"
import fs from 'fs-extra'

import favicons from './favicons'
import path from 'path'
import hljs from 'highlight.js'
import MarkdownIt from 'markdown-it'
import {Dirent} from "fs"
import sizeOf from 'image-size'

const md = MarkdownIt({
  langPrefix: 'lang-',
  highlight: function (content: string, lang: string) {
    if (lang && hljs.getLanguage(lang)) {
      return `<pre><code class="hljs">${hljs.highlight(content, {language: lang}).value}</code></pre>`
    }

    return `<pre><code class="hljs">${md.utils.escapeHtml(content)}</code></pre>`
  }
})

export interface Page {
  srcFilePath: string
  distFilePath: string
  baseUri: string
  uri: string
}

type ArticleMeta = { title: string, description: string }

export interface Article extends Page {
  title: string
  description: string
  date: Date
  srcDir: string
}

export type Document = Page | Article

export function isArticle(page: Page | Article): page is Article {
  return page.hasOwnProperty("date")
}

const viewsDir = srcDir('views')

const pugFileFilter = (f: Dirent): boolean => f.isFile() && f.name.endsWith('.pug') && !f.name.startsWith('_')

const pugFiles = (dir: string): string[] =>
  fs.readdirSync(dir, {withFileTypes: true}).filter(pugFileFilter).map(f => path.join(dir, f.name))

export const pages = (): Page[] =>
  pugFiles(viewsDir).map(filePath => {
      const basename = path.basename(filePath, '.pug')

      return {
        srcFilePath: filePath,
        distFilePath: `${basename}.html`,
        baseUri: '/',
        uri: basename == 'index' ? '/' : `/${basename}.html`
      }
    }
  )

export const articles = (): Article[] => {
  const basePath = path.join(viewsDir, 'articles')

  return fs.readdirSync(basePath).flatMap(year => {
      return fs.readdirSync(path.join(basePath, year))
        .flatMap(month =>
          fs.readdirSync(path.join(basePath, year, month)).map(day => {
              const srcDir = path.join(basePath, year, month, day)
              const baseDist = path.join('blog', year, month, day)
              const baseUri = `/blog/${year}/${month}/${day}/`
              const srcFilePath = pugFiles(srcDir)[0]
              const basename = path.basename(srcFilePath, '.pug')
              const htmlFileName = `${basename}.html`
              const meta = JSON.parse(fs.readFileSync(path.join(srcDir, 'meta.json')).toString()) as ArticleMeta

              return {
                title: meta.title,
                description: meta.description,
                date: new Date(Date.UTC(parseInt(year), parseInt(month) - 1, parseInt(day), 0)),
                srcDir: srcDir,
                srcFilePath: srcFilePath,
                distFilePath: path.join(baseDist, htmlFileName),
                baseUri: baseUri,
                uri: `${baseUri}${htmlFileName}`
              }
            }
          )
        )
    }
  ).sort((a, b) => b.date.getTime() - a.date.getTime())
}

export const allDocuments = (): (Page | Article)[] => pages().concat(articles())

const toBase64ImgUrl = (fileName: string): string => {
  let imgType = fileName.split('.')[1]
  if (imgType == 'jpg') imgType = 'jpeg'

  const filePath = `assets/${fileName}`
  return `data:image/${imgType};base64,${fs.readFileSync(filePath, 'base64')}`
}

export function data(isProduction: boolean) {
  return {
    assetSrc(name: string) {
      return 'assets/' + name;
    },

    faviconsHtml(): string {
      const content = fs.readFileSync(favicons.output.html).toString()
      return isProduction ? content.replace(/\n/g, "") : content
    },

    data: (): JSON => JSON.parse(fs.readFileSync('views/data/data.json').toString()),

    readCss: (name: string): string => fs.readFileSync(buildDir(`css/${name}.css`)).toString(),

    readAsset: (fileName: string): string => fs.readFileSync(buildDir(`assets/${fileName}`)).toString(),

    imageAttributes(fileName: string, asBase64: boolean = false) {
      const filePath = `assets/${fileName}`
      const size = sizeOf(filePath)
      const src = asBase64 ? toBase64ImgUrl(fileName) : filePath;
      return {src, width: size.width, height: size.height}
    },

    readSvg: (name: string): string => fs.readFileSync(buildDir(`assets/${name}.svg`)).toString(),

    currentTimestamp() {
      return new Date().getTime()
    },

    articles,

    toLocaleDate: (date: Date): string => date.toLocaleDateString(
      'en-GB', {year: 'numeric', month: 'long', day: 'numeric'}
    ),

    toIsoDate: (date: Date): string => date.toISOString().slice(0, 10),

    articleData(article: Article) {
      return {
        parseArticleMd: (mdFilePath: string): string => {
          md.renderer.rules.image = function (tokens, idx,opts, env, renderer) {
            tokens = tokens.map(token => {
              const imgSize = token.attrs.filter(a => a[0] == 'src')
                .map(srcAttr => {
                  const articleDir = buildDir(path.dirname(article.distFilePath))
                  const imgPath = path.join(articleDir, srcAttr[1])
                  return sizeOf(imgPath)
                })[0]

              token.attrs.push(['width', imgSize.width])
              token.attrs.push(['height', imgSize.height])
              return token;
            })

            return renderer.renderToken(tokens, idx, opts)
          }

          return md.render(fs.readFileSync(path.join(article.srcDir, mdFilePath)).toString())
        },
        articleTitle: article.title,
        articleDescription: article.description,
        articleDate: article.date,
        articleBaseUri: article.baseUri
      }
    }
  }
}
