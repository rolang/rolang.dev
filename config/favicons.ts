import {buildDir, srcDir} from "./paths"
import rfgConfig from 'rfg-config'

const pkg = require('../package')
const iconsPath = '/assets/favicons'

export default {
  src: srcDir('assets', 'logo.png'),
  options: Object.assign(rfgConfig.defaultConf, {
    path: iconsPath,
    appName: pkg.name,
    appDescription: pkg.description,
    version: pkg.version,
    icons: {
      android: false,
      appleIcon: false,
      appleStartup: false,
      coast: false,
      favicons: true,
      firefox: false,
      windows: false,
      yandex: false
    }
  }),
  output: {
    files: buildDir(iconsPath),
    html: buildDir('favicons.html')
  }
}
