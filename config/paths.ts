import path from 'path'

function dir(base: string, args: string[]): string {
  return path.join(...(base.split('/').concat(args.flatMap(s => s.split('/')))));
}

const build = 'dist'
const packages = '.'

export const buildDir = (...args: string[]) => dir(build, args)
export const srcDir = (...args: string[]) => dir(packages, args)
export const sassDir = (...args: string[]) => dir(path.join(packages, 'sass'), args)
export const cssDir = (...args: string[]) => dir(path.join(build, 'css'), args)